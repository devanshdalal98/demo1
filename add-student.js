$('input[type="checkbox"]').on('change', function() {
		$(this).siblings('input[type="checkbox"]').prop('checked', false);
});

$('.datepicker').datepicker({
    format: 'mm/dd/yyyy',
    startDate: '-3d'
});